Feature: Broadcast

  Background:
    Given the internet is reachable
    And an initialised environment
    And the system is bootstrapped

  Scenario: A command is issued with no prior Broadcast received
    Given no prior Broadcast was received
    And a new Broadcast "This is a LIVE Broadcast!" with id "12345" is available
    When I enter "sdk version"
    Then I see "This is a LIVE Broadcast!"

  Scenario: A command is issued where the prior Broadcast was different to the Live one
    Given a prior Broadcast "This is an OLD Broadcast!" with id "12344" was issued
    And a new Broadcast "This is a LIVE Broadcast!" with id "12345" is available
    When I enter "sdk version"
    Then I see "This is a LIVE Broadcast!"

  Scenario: A command is issued where the prior Broadcast was the same as the Live one
    Given a prior Broadcast "This is a LIVE Broadcast!" with id "12345" was issued
    And a new Broadcast "This is a LIVE Broadcast!" with id "12345" is available
    When I enter "sdk version"
    Then I do not see "This is a LIVE Broadcast!"

  Scenario: A Broadcast command recalls a prior Broadcast
    Given a prior Broadcast "This is an OLD Broadcast!" with id "12344" was issued
    And a new Broadcast "This is an OLD Broadcast!" with id "12344" is available
    When I enter "sdk broadcast"
    Then I see "This is an OLD Broadcast!"

  Scenario: A Broadcast command is issued with no prior Broadcast received
    Given no prior Broadcast was received
    And a new Broadcast "This is a LIVE Broadcast!" with id "12345" is available
    When I enter "sdk broadcast"
    Then I see "This is a LIVE Broadcast!"

Feature: Current Candidate

  Background:
    Given the internet is reachable
    And an initialised environment

  Scenario: Display current candidate version in use
    Given the candidate "grails" version "1.3.9" is already installed and default
    And the system is bootstrapped
    When I enter "sdk current grails"
    Then I see "Using grails version 1.3.9"

  Scenario: Display current candidate version when none is in use
    Given the candidate "grails" version "1.3.9" is already installed but not default
    And the system is bootstrapped
    When I enter "sdk current grails"
    Then I see "Not using any version of grails"

  Scenario: Display current candidate versions when none is specified and none is in use
    Given the candidate "grails" version "1.3.9" is already installed but not default
    And the system is bootstrapped
    When I enter "sdk current"
    Then I see "No candidates are in use"

  Scenario: Display current candidate versions when none is specified and one is in use
    Given the candidate "grails" version "2.1.0" is already installed and default
    And the system is bootstrapped
    When I enter "sdk current"
    Then I see "Using:"
    And I see "grails: 2.1.0"

  Scenario: Display current candidate versions when none is specified and multiple are in use
    Given the candidate "groovy" version "2.0.5" is already installed and default
    And the candidate "grails" version "2.1.0" is already installed and default
    And the system is bootstrapped
    When I enter "sdk current"
    Then I see "Using:"
    And I see "grails: 2.1.0"
    And I see "groovy: 2.0.5"

Feature: Flush

  Background:
    Given the internet is reachable
    And an initialised environment
    And the system is bootstrapped

  Scenario: Flush omitting the Qualifier
    When I enter "sdk flush"
    Then I see "Stop! Please specify what you want to flush."

  Scenario: Clean up the current Broadcast
    Given a prior Broadcast "This is an old broadcast" with id "12344" was issued
    When I enter "sdk flush broadcast"
    Then no broadcast message can be found
    And I see "Broadcast has been flushed."

  Scenario: Clean up an uninitialised Broadcast
    Given the broadcast has been flushed
    When I enter "sdk flush broadcast"
    Then I see "No prior broadcast found so not flushed."

  Scenario: Clean up the last known Remote Version
    Given a prior version "5.0.0" was detected
    When I enter "sdk flush version"
    Then no version file can be found
    And I see "Version file has been flushed."

  Scenario: Clean up an uninitialised last known Remote Version
    Given the Remote Version has been flushed
    When I enter "sdk flush version"
    Then I see "No prior Remote Version found so not flushed."

  Scenario: Clear out the cached Archives
    Given the archive "grails-1.3.9.zip" has been cached
    When I enter "sdk flush archives"
    Then no archives are cached
    And I see "1 archive(s) flushed"

  Scenario: Clear out the temporary space
    Given the file "res-1.2.0.zip" in temporary storage
    When I enter "sdk flush temp"
    Then no "res-1.2.0.zip" file is present in temporary storage
    And I see "1 archive(s) flushed"
Feature: Install Candidate

  Background:
    Given the internet is reachable
    And an initialised environment

  Scenario: Install a default Candidate and set to default
    Given the system is bootstrapped
    And the candidate "grails" version "2.1.0" is a valid candidate version
    And the default "grails" version is "2.1.0"
    When I enter "sdk install grails"
    Then I see "Done installing!"
    And I do not see "Do you want grails 2.1.0 to be set as default? (Y/n)"
    And the candidate "grails" version "2.1.0" is installed

  Scenario: Install a specific Candidate and set to default
    Given the system is bootstrapped
    And the candidate "grails" version "1.3.9" is available for download
    When I enter "sdk install grails 1.3.9"
    Then I see "Done installing!"
    And I do not see "Do you want grails 2.1.0 to be set as default? (Y/n)"
    And the candidate "grails" version "1.3.9" is installed

  Scenario: Install a Candidate version that does not exist
    Given the system is bootstrapped
    And the candidate "grails" version "1.4.4" is not available for download
    When I enter "sdk install grails 1.4.4"
    Then I see "Stop! grails 1.4.4 is not available."
    And the exit code is 1

  Scenario: Install a Candidate version that is already installed
    Given the system is bootstrapped
    And the candidate "grails" version "1.3.9" is available for download
    And the candidate "grails" version "1.3.9" is already installed and default
    When I enter "sdk install grails 1.3.9"
    Then I see "Stop! grails 1.3.9 is already installed."

  Scenario: Install a candidate and auto-answer to make it default
    Given the system is bootstrapped
    And the candidate "grails" version "2.1.0" is available for download
    And I have configured "sdkman_auto_answer" to "true"
    When I enter "sdk install grails 2.1.0"
    Then the candidate "grails" version "2.1.0" is installed
    And I do not see "Do you want grails 2.1.0 to be set as default?"
    And I see "Done installing!"
    And I see "Setting grails 2.1.0 as default."
    And the candidate "grails" version "2.1.0" should be the default

  Scenario: Install a candidate and choose to make it default
    Given the candidate "grails" version "1.3.9" is already installed and default
    And the system is bootstrapped
    And the candidate "grails" version "2.1.0" is available for download
    When I enter "sdk install grails 2.1.0" and answer "Y"
    Then the candidate "grails" version "2.1.0" is installed
    And I see "Done installing!"
    And I see "Do you want grails 2.1.0 to be set as default? (Y/n)"
    And I see "Setting grails 2.1.0 as default."
    And the candidate "grails" version "2.1.0" should be the default
    And the candidate "grails" version "1.3.9" should not be the default

  Scenario: Install a candidate and choose not to make it default
    Given the candidate "grails" version "1.3.9" is already installed and default
    And the system is bootstrapped
    And the candidate "grails" version "2.1.0" is available for download
    When I enter "sdk install grails 2.1.0" and answer "n"
    Then the candidate "grails" version "2.1.0" is installed
    And I see "Done installing!"
    And I see "Do you want grails 2.1.0 to be set as default? (Y/n)"
    And I do not see "Setting grails 2.1.0 as default."
    And the candidate "grails" version "2.1.0" should not be the default
    And the candidate "grails" version "1.3.9" should be the default

  #revisit to redownload automatically
  Scenario: Abort installation on download of a corrupt Candidate archive
    Given the system is bootstrapped
    And the candidate "grails" version "1.3.6" is available for download
    And the archive for candidate "grails" version "1.3.6" is corrupt
    When I enter "sdk install grails 1.3.6"
    Then I see "Stop! The archive was corrupt and has been removed! Please try installing again."
    And the candidate "grails" version "1.3.6" is not installed
    And the archive for candidate "grails" version "1.3.6" is removed
Feature: Java Multi Platform Binary Distribution

  Three versions of Java are used to test various installation scenarios.
  This feature uses real hooks found in the resources folder under /hooks.

  The following hooks are available:
  8u111: pre- and post-hooks prepared for successful installation
  8u101: pre-hook succeeds but post-hook aborts with non-zero return code
  8u92 : pre-hook aborts with non-zero return code and post-hook is ignored

  In order to install Oracle Java, the user needs to agree to the Oracle Binary Code License Agreement
  This License can be found at: http://www.oracle.com/technetwork/java/javase/terms/license/index.html

  Background:
    Given the internet is reachable
    And an initialised environment

  Scenario: Platform is supported and a specific version of compatible binary is installed
    Given a machine with "Linux" installed
    And the system is bootstrapped
    And the candidate "java" version "8u111" is available for download on "Linux"
    And a cookie is required for installing "java" "8u111" on "Linux"
    And the appropriate multi-platform hooks are available for "java" version "8u111" on "Linux"
    When I enter "sdk install java 8u111", accept the license agreement and confirm to make this the default installation
    Then a download request was made for "java" "8u111" on "Linux" with cookie "oraclelicense=accept-securebackup-cookie"
    And I see "Done installing!"
    And the candidate "java" version "8u111" is installed
    And the cookie has been removed

  Scenario: Platform is supported and a default version of compatible binary is installed
    Given a machine with "Linux" installed
    And the system is bootstrapped
    And the default "java" version is "8u111"
    And the candidate "java" version "8u111" is available for download on "Linux"
    And a cookie is required for installing "java" "8u111" on "Linux"
    And the appropriate multi-platform hooks are available for "java" version "8u111" on "Linux"
    When I enter "sdk install java", accept the license agreement and confirm to make this the default installation
    Then a download request was made for "java" "8u111" on "Linux" with cookie "oraclelicense=accept-securebackup-cookie"
    And I see "Done installing!"
    And the candidate "java" version "8u111" is installed
    And the cookie has been removed

  Scenario: Platform is supported but the User does not agree with the Oracle Binary Code License Agreement
    And a machine with "Linux" installed
    And the system is bootstrapped
    And the candidate "java" version "8u92" is available for download on "Linux"
    And the appropriate multi-platform hooks are available for "java" version "8u92" on "Linux"
    When I enter "sdk install java 8u92" and do not accept the license agreement
    Then I see "Do you agree to the terms of this agreement? (Y/n)"
    And I see "http://www.oracle.com/technetwork/java/javase/terms/license/index.html"
    And I see "Not installing java 8u92 at this time..."
    And the candidate "java" version "8u92" is not installed

  Scenario: Platform is supported but download fails
    Given a machine with "Linux" installed
    And the system is bootstrapped
    And the candidate "java" version "8u101" is available for download on "Linux"
    And a cookie is required for installing "java" "8u101" on "Linux"
    And the appropriate multi-platform hooks are available for "java" version "8u101" on "Linux"
    When I enter "sdk install java 8u101" and accept the license agreement
    Then a download request was made for "java" "8u101" on "Linux" with cookie "oraclelicense=accept-securebackup-cookie"
    And I see "Download has failed, aborting!"
    And the candidate "java" version "8u101" is not installed
    And I see "Can not install java 8u101 at this time..."
    And the cookie has been removed

  Scenario: Platform is not supported for specific version and user is notified
    And a machine with "FreeBSD" installed
    And the system is bootstrapped
    And the candidate "java" version "8u111" is not available for download on "FreeBSD"
    When I enter "sdk install java 8u111"
    Then I see "Stop! java 8u111 is not available. Possible causes:"
    And I see " * 8u111 is an invalid version"
    And I see " * java binaries are incompatible with FreeBSD"
    And I see " * java has not been released yet"
    And the candidate "java" version "8u111" is not installed

  Scenario: Platform is not supported for default version and user is notified
    And a machine with "FreeBSD" installed
    And the system is bootstrapped
    And the default "java" version is "8u111"
    And the candidate "java" version "8u111" is not available for download on "FreeBSD"
    When I enter "sdk install java"
    Then I see "Stop! java 8u111 is not available. Possible causes:"
    And I see " * 8u111 is an invalid version"
    And I see " * java binaries are incompatible with FreeBSD"
    And I see " * java has not been released yet"
    And the candidate "java" version "8u111" is not installed
Feature: Local Development Versions

  Background:
    Given the internet is reachable
    And an initialised environment

  Scenario: Install a new local development version
    Given the candidate "groovy" version "2.1-SNAPSHOT" is not available for download
    And I have a local candidate "groovy" version "2.1-SNAPSHOT" at "/tmp/groovy-core"
    And the system is bootstrapped
    When I enter "sdk install groovy 2.1-SNAPSHOT /tmp/groovy-core"
    Then I see "Linking groovy 2.1-SNAPSHOT to /tmp/groovy-core"
    And the candidate "groovy" version "2.1-SNAPSHOT" is linked to "/tmp/groovy-core"

  Scenario: Attempt installing a local development version that already exists
    Given the candidate "groovy" version "2.1-SNAPSHOT" is not available for download
    And the candidate "groovy" version "2.1-SNAPSHOT" is already linked to "/tmp/groovy-core"
    And the system is bootstrapped
    When I enter "sdk install groovy 2.1-SNAPSHOT /tmp/groovy-core"
    Then I see "Stop! groovy 2.1-SNAPSHOT is already installed."
    And the candidate "groovy" version "2.1-SNAPSHOT" is linked to "/tmp/groovy-core"

  Scenario: Uninstall a local development version
    Given the candidate "groovy" version "2.1-SNAPSHOT" is already linked to "/tmp/groovy-core"
    And the system is bootstrapped
    When I enter "sdk uninstall groovy 2.1-SNAPSHOT"
    Then I see "Uninstalling groovy 2.1-SNAPSHOT"
    And the candidate "groovy" version "2.1-SNAPSHOT" is not installed

  Scenario: Attempt uninstalling a local development version that is not installed
    Given the candidate "groovy" version "2.1-SNAPSHOT" is not installed
    And the system is bootstrapped
    When I enter "sdk uninstall groovy 2.1-SNAPSHOT"
    Then I see "groovy 2.1-SNAPSHOT is not installed."

  Scenario: Make the local development version the default for the candidate
    Given the candidate "groovy" version "2.0.6" is already installed and default
    And the candidate "groovy" version "2.1-SNAPSHOT" is not available for download
    And the candidate "groovy" version "2.1-SNAPSHOT" is already linked to "/tmp/groovy-core"
    And the system is bootstrapped
    When I enter "sdk default groovy 2.1-SNAPSHOT"
    Then I see "Default groovy version set to 2.1-SNAPSHOT"
    And the candidate "groovy" version "2.1-SNAPSHOT" should be the default

  Scenario: Use a local development version
    Given the candidate "groovy" version "2.0.6" is already installed and default
    And the candidate "groovy" version "2.1-SNAPSHOT" is not available for download
    And the candidate "groovy" version "2.1-SNAPSHOT" is already linked to "/tmp/groovy-core"
    And the system is bootstrapped
    When I enter "sdk use groovy 2.1-SNAPSHOT"
    Then I see "Using groovy version 2.1-SNAPSHOT in this shell"
    And the candidate "groovy" version "2.1-SNAPSHOT" should be in use

  Scenario: Install a local development version from a valid relative path
    Given the candidate "groovy" version "2.1-SNAPSHOT" is not available for download
    And I have a local candidate "groovy" version "2.1-SNAPSHOT" at relative path "some/relative/path/to/groovy"
    And the system is bootstrapped
    When I enter "sdk install groovy 2.1-SNAPSHOT some/relative/path/to/groovy"
    Then I see "Linking groovy 2.1-SNAPSHOT"
    And the candidate "groovy" version "2.1-SNAPSHOT" is linked to the relative path "some/relative/path/to/groovy"

  Scenario: Prevent installation of a local development version for an invalid path
    Given the candidate "groovy" version "2.1-SNAPSHOT" is not available for download
    And the system is bootstrapped
    When I enter "sdk install groovy 2.1-SNAPSHOT /some/bogus/path/to/groovy"
    Then I see "Invalid path! Refusing to link groovy 2.1-SNAPSHOT to /some/bogus/path/to/groovy."
    And the candidate "groovy" version "2.1-SNAPSHOT" is not installed

  Scenario: Prevent installation of a local development version for a long version
    Given the candidate "groovy" version "2.1-SNAPSHOTLONG" is not available for download
    And I have a local candidate "groovy" version "2.1-SNAPSHOTLONG" at relative path "some/relative/path/to/groovy"
    And the system is bootstrapped
    When I enter "sdk install groovy 2.1-SNAPSHOTLONG some/relative/path/to/groovy"
    Then I see "Invalid version! 2.1-SNAPSHOTLONG with length 16 exceeds max of 15!"
    And the candidate "groovy" version "2.1-SNAPSHOTLONG" is not installed

  Scenario: Allow installation of a local development version for longest possible version
    Given the candidate "groovy" version "2.1-SNAPSHOT-XX" is not available for download
    And I have a local candidate "groovy" version "2.1-SNAPSHOT-XX" at "/tmp/groovy-core"
    And the system is bootstrapped
    When I enter "sdk install groovy 2.1-SNAPSHOT-XX /tmp/groovy-core"
    Then I see "Linking groovy 2.1-SNAPSHOT-XX to /tmp/groovy-core"
    And the candidate "groovy" version "2.1-SNAPSHOT-XX" is linked to "/tmp/groovy-core"

  Scenario: Allow installation of a local development version for a short version
    Given the candidate "java" version "graal" is not available for download
    And I have a local candidate "java" version "graal" at "/tmp/graalvm-1.0.0-rc4-graal"
    And the system is bootstrapped
    When I enter "sdk install java graal /tmp/graalvm-1.0.0-rc4-graal"
    Then I see "Linking java graal to /tmp/graalvm-1.0.0-rc4-graal"
    And the candidate "java" version "graal" is linked to "/tmp/graalvm-1.0.0-rc4-graal"
Feature: Offline Mode

  #offline modes

  Scenario: Enter an invalid offline mode
    Given offline mode is disabled with reachable internet
    And an initialised environment
    And the system is bootstrapped
    When I enter "sdk offline grails"
    Then I see "Stop! grails is not a valid offline mode."

  Scenario: Issue Offline command without qualification
    Given offline mode is disabled with reachable internet
    And an initialised environment
    And the system is bootstrapped
    When I enter "sdk offline"
    Then I see "Offline mode enabled."

  Scenario: Enable Offline Mode with internet reachable
    Given offline mode is disabled with reachable internet
    And an initialised environment
    And the system is bootstrapped
    When I enter "sdk offline enable"
    Then I see "Offline mode enabled."
    And I do not see "INTERNET NOT REACHABLE!"
    When I enter "sdk install grails 2.1.0"
    Then I do not see "INTERNET NOT REACHABLE!"
    And I see "Stop! grails 2.1.0 is not available while offline."

  Scenario: Disable Offline Mode with internet reachable
    Given offline mode is enabled with reachable internet
    And the candidate "grails" version "2.1.0" is available for download
    And an initialised environment
    And the system is bootstrapped
    When I enter "sdk offline disable"
    Then I see "Online mode re-enabled!"
    When I enter "sdk install grails 2.1.0" and answer "Y"
    Then I see "Done installing!"
    And the candidate "grails" version "2.1.0" is installed

  Scenario: Disable Offline Mode with internet unreachable
    Given offline mode is enabled with unreachable internet
    And an initialised environment
    And the system is bootstrapped
    When I enter "sdk offline disable"
    Then I see "Online mode re-enabled!"
    When I enter "sdk install grails 2.1.0"
    Then I see "INTERNET NOT REACHABLE!"
    And I see "Stop! grails 2.1.0 is not available while offline."

  #broadcast
  Scenario: Recall a broadcast while in Offline Mode
    Given offline mode is enabled with reachable internet
    And an initialised environment
    And the system is bootstrapped
    When a prior Broadcast "This is an OLD Broadcast!" with id "12344" was issued
    And I enter "sdk broadcast"
    Then I see "This is an OLD Broadcast!"

  #sdk version
  Scenario: Determine the sdkman version while in Offline Mode
    Given offline mode is enabled with reachable internet
    And an initialised environment
    And the system is bootstrapped
    When I enter "sdk version"
    Then I see the current sdkman version

  #list candidate version
  Scenario: List candidate versions found while in Offline Mode
    Given offline mode is enabled with reachable internet
    And an initialised environment
    And the system is bootstrapped
    When I enter "sdk list grails"
    Then I see "Offline: only showing installed grails versions"

  #use version
  Scenario: Use an uninstalled candidate version while in Offline Mode
    Given offline mode is enabled with reachable internet
    And the candidate "grails" version "1.3.9" is already installed and default
    And the candidate "grails" version "2.1.0" is not installed
    And an initialised environment
    And the system is bootstrapped
    When I enter "sdk use grails 2.1.0"
    Then I see "Stop! grails 2.1.0 is not available while offline."

  #default version
  Scenario: Set the default to an uninstalled candidate version while in Offline Mode
    Given offline mode is enabled with reachable internet
    And the candidate "grails" version "1.3.9" is already installed and default
    And an initialised environment
    And the system is bootstrapped
    When I enter "sdk default grails 2.1.0"
    Then I see "Stop! grails 2.1.0 is not available while offline."

  #install command
  Scenario: Install a candidate version that is not installed while in Offline Mode
    Given offline mode is enabled with reachable internet
    And the candidate "grails" version "2.1.0" is not installed
    And an initialised environment
    And the system is bootstrapped
    When I enter "sdk install grails 2.1.0"
    Then I see "Stop! grails 2.1.0 is not available while offline."

  #uninstall command
  Scenario: Uninstall a candidate version while in Offline Mode
    Given offline mode is enabled with reachable internet
    And the candidate "grails" version "2.1.0" is already installed and default
    And an initialised environment
    And the system is bootstrapped
    When I enter "sdk uninstall grails 2.1.0"
    And the candidate "grails" version "2.1.0" is not installed

  #current command
  Scenario: Display the current version of a candidate while in Offline Mode
    Given offline mode is enabled with reachable internet
    And the candidate "grails" version "2.1.0" is already installed and default
    And an initialised environment
    And the system is bootstrapped
    When I enter "sdk current grails"
    Then I see "Using grails version 2.1.0"

  #help command
  Scenario: Request help while in Offline Mode
    Given offline mode is enabled with reachable internet
    And an initialised environment
    And the system is bootstrapped
    When I enter "sdk help"
    Then I see "Usage: sdk <command> [candidate] [version]"

  #selfupdate command
  Scenario: Attempt self-update while in Offline Mode
    Given offline mode is enabled with reachable internet
    And an initialised environment
    And the system is bootstrapped
    When I enter "sdk selfupdate"
    Then I see "This command is not available while offline."

Feature: Service Unavailable

  Background:
    Given the internet is not reachable
    And an initialised environment

  # list commands

  Scenario: List candidate versions found while Offline
    Given the candidate "grails" version "2.1.0" is already installed and default
    And the candidate "grails" version "1.3.9" is already installed but not default
    And the system is bootstrapped
    When I enter "sdk list grails"
    Then I see "Offline: only showing installed grails versions"
    And I see "> 2.1.0"
    And I see "* 1.3.9"

  Scenario: List candidate versions not found while Offline
    Given the system is bootstrapped
    When I enter "sdk list grails"
    Then I see "Offline: only showing installed grails versions"
    And I see "None installed!"

  Scenario: List Available Candidates while Offline
    Given the system is bootstrapped
    When I enter "sdk list"
    Then I see "This command is not available while offline."

# use command

  Scenario: Use the default candidate version while Offline
    Given the candidate "grails" version "2.1.0" is already installed and default
    And the candidate "grails" version "1.3.9" is already installed but not default
    And the system is bootstrapped
    When I enter "sdk use grails"
    Then I see "Using grails version 2.1.0 in this shell."

  Scenario: Use the default candidate version when non selected while Offline
    Given the candidate "grails" version "1.3.9" is already installed but not default
    And the candidate "grails" version "2.1.0" is already installed but not default
    And the system is bootstrapped
    When I enter "sdk use grails"
    Then I see "This command is not available while offline."

  Scenario: Use an uninstalled candidate version while Offline
    Given the candidate "grails" version "1.3.9" is already installed and default
    And the candidate "grails" version "2.1.0" is not installed
    And the system is bootstrapped
    When I enter "sdk use grails 2.1.0"
    Then I see "Stop! grails 2.1.0 is not available while offline."

  Scenario: Use an invalid candidate version while Offline
    Given the candidate "grails" version "1.3.9" is already installed and default
    And the system is bootstrapped
    When I enter "sdk use grails 9.9.9"
    Then I see "Stop! grails 9.9.9 is not available while offline."

  Scenario: Use an installed candidate version while Offline
    Given the candidate "grails" version "2.1.0" is already installed and default
    And the candidate "grails" version "1.3.9" is already installed but not default
    And the system is bootstrapped
    When I enter "sdk use grails 1.3.9"
    Then I see "Using grails version 1.3.9 in this shell."

  # default command

  Scenario: Set the default to an uninstalled candidate version while Offline
    Given the candidate "grails" version "1.3.9" is already installed and default
    And the system is bootstrapped
    When I enter "sdk default grails 2.1.0"
    Then I see "Stop! grails 2.1.0 is not available while offline."

  Scenario: Set the default to an invalid candidate version while Offline
    Given the candidate "grails" version "1.3.9" is already installed and default
    And the system is bootstrapped
    When I enter "sdk default grails 999"
    Then I see "Stop! grails 999 is not available while offline."

  Scenario: Set the default to an installed candidate version while Offline
    Given the candidate "grails" version "2.1.0" is already installed and default
    And the candidate "grails" version "1.3.9" is already installed but not default
    And the system is bootstrapped
    When I enter "sdk default grails 1.3.9"
    Then I see "Default grails version set to 1.3.9"

  # install command
  Scenario: Install a candidate version that is not installed while Offline
    Given the candidate "grails" version "2.1.0" is not installed
    And the system is bootstrapped
    When I enter "sdk install grails 2.1.0"
    Then I see "Stop! grails 2.1.0 is not available while offline."

  Scenario: Install a candidate version that is already installed while Offline
    Given the candidate "grails" version "2.1.0" is already installed and default
    And the system is bootstrapped
    When I enter "sdk install grails 2.1.0"
    Then I see "Stop! grails 2.1.0 is already installed."

  # uninstall command
  Scenario: Uninstall a candidate version while Offline
    Given the candidate "grails" version "2.1.0" is already installed and default
    And the system is bootstrapped
    When I enter "sdk uninstall grails 2.1.0"
    Then I see "Unselecting grails 2.1.0..."
    And I see "Uninstalling grails 2.1.0..."
    And the candidate "grails" version "2.1.0" is not in use
    And the candidate "grails" version "2.1.0" is not installed

  Scenario: Uninstall a candidate version that is not installed while Offline
    Given the candidate "grails" version "2.1.0" is not installed
    And the system is bootstrapped
    When I enter "sdk uninstall grails 2.1.0"
    Then I see "grails 2.1.0 is not installed."

  # current command
  Scenario: Display the current version of a candidate while Offline
    Given the candidate "grails" version "2.1.0" is already installed and default
    And the system is bootstrapped
    When I enter "sdk current grails"
    Then I see "Using grails version 2.1.0"

  Scenario: Display the current version of all candidates while Offline
    Given the candidate "grails" version "2.1.0" is already installed and default
    And the candidate "groovy" version "2.0.5" is already installed and default
    And the system is bootstrapped
    When I enter "sdk current"
	Then I see "Using:"
	And I see "grails: 2.1.0"
	And I see "groovy: 2.0.5"

  # version command
  Scenario: Determine the sdkman version when Offline
    Given the system is bootstrapped
    When I enter "sdk version"
    Then I see the current sdkman version

  # broadcast command
  Scenario: Recall a broadcast while Offline
    Given a prior Broadcast "This is an OLD Broadcast!" with id "12344" was issued
    And the system is bootstrapped
    When I enter "sdk broadcast"
    Then I see "This is an OLD Broadcast!"

  # help command
  Scenario: Request help while Offline
    Given the system is bootstrapped
    When I enter "sdk help"
    Then I see "Usage: sdk <command> [candidate] [version]"

  # selfupdate command
  Scenario: Attempt self-update while Offline
    Given the system is bootstrapped
    When I enter "sdk selfupdate"
    Then I see "This command is not available while offline."

