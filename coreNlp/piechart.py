import dash
import dash_core_components as dcc
import dash_html_components as html
import plotly.plotly as py
import plotly.graph_objs as go
import json
import urllib2
import requests


# gl=gitlab.Gitlab('https://gitlab.com/stefsot/', private_token='dsUx3WCFyyCLk9GwJz8w')
# gl.auth()
#
# project = gl.projects.get('stefsot%2Fmsc-it-project%2FcoreNlp', lazy=True)
# items = project.repository_tree()
#
# print(items)

# id = [d['id'] for d in p.repository_tree() if d['name'] == 'dataGraph.json'][0]
# file_content = p.repository_raw_blob(id)
# print(str(file_content))
headers = {
    'PRIVATE_TOKEN': 'dsUx3WCFyyCLk9GwJz8w',
}

response = requests.get('https://gitlab.com/stefsot/msc-it-project/raw/master/coreNlp/dataGraph.json', headers=headers)

mydata= response.content


# with open('dataGraph.json', 'r') as myfile:
#     data=myfile.read()


obj =json.loads(mydata)
labels=list()
values=list()
for a in obj:
        print("passive voice: " + str(a['passive voice']))
        print("active voice: " + str(a['active voice']))
            # labels= [a.keys()]
            # values= [a.values()]
        for b in a.keys():
            labels.append(b)

        for c in a.values():
            values.append(c)

        # labels = ['Oxygen','Hydrogen','Carbon_Dioxide','Nitrogen']
        # values = [4500,2500,1053,500]

trace = go.Pie(labels=labels, values=values)

py.iplot([trace], filename='basic_pie_chart')
# app= dash.Dash()
#
#
# app.layout = html.Div([
#     dcc.Graph(id='my-graph')
#              ], style={'width': '60', 'display': 'inline-block'})
#
#
#
# @app.callback(
# dash.dependencies.Output('my-graph', 'figure'))
# def display_content():
#
#     trace = go.Pie(labels=labels, values=values)
#     return {
#         'data':[trace],
#         'layout': {'height': 250,'margin': {'l': 20, 'b': 30, 'r': 10, 't': 10}}
# }
#
# if __name__ == '__main__':
#     app.run_server(debug=True)
