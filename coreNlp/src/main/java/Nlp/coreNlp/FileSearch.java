package Nlp.coreNlp;

	import java.io.File;
	import java.util.ArrayList;
	import java.util.List;

	public class FileSearch {

	  private String fileNameToSearch;
	  private List<String> result = new ArrayList<String>();
	  private List<File> resultFile=new ArrayList<File>();
	  private int counter=0;
		
	  public String getFileNameToSearch() {
		return fileNameToSearch;
	  }

	  public void setFileNameToSearch(String fileNameToSearch) {
		this.fileNameToSearch = fileNameToSearch;
	  }

	  public List<String> getResult() {
		return result;
	  }
	  public List<File>getResultFile(){
		  return resultFile;
	  }

	  public FileSearch() {
		searchDirectory(new File("C:\\Users\\sotir_000\\eclipse-workspace\\coreNlp\\repos"), ".feature");
		// Change directory with the repos directory in your hardware
		int count = getResult().size();
		if(count ==0){
			//if results come empty 
		    System.out.println("\nNo result found!");
		}else{
			//if results is not null
			System.out.println(counter);
		    System.out.println("\nFound " + count + " result!\n");
		    for (String matched : getResult()){
		    	System.out.println("Found : " + matched);
		    }
		}
	  }

	  public void searchDirectory(File directory, String fileNameToSearch) {
		/* Given specific directory and file name 
		 * searches the directory for files with that name in their title*/
		setFileNameToSearch(fileNameToSearch);

		if (directory.isDirectory()) {
		    search(directory);
		} else {
			//if directory doesn't exist
		    System.out.println(directory.getAbsoluteFile() + " is not a directory!");
		}

	  }

	  private void search(File file) {
		  
		if (file.isDirectory()) {
		  System.out.println("Searching directory ... " + file.getAbsoluteFile());
			counter++;
	            //do you have permission to read this directory?	
		    if (file.canRead()) {
			for (File temp : file.listFiles()) {
			    if (temp.isDirectory()) {
				search(temp);
			    } else {

			    if (temp.getName().endsWith(fileNameToSearch)) {
			    	result.add(temp.getAbsoluteFile().toString());
			    	resultFile.add(temp);
			    }	
			}
		    }

		 } else {
			System.out.println(file.getAbsoluteFile() + "Permission Denied");
		 }
	      }

	  }

	}

