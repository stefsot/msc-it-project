package Nlp.coreNlp;

import java.util.Collection;
import java.util.List;

import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.pipeline.CoreNLPProtos.Sentence;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.PennTreebankLanguagePack;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.stanford.nlp.trees.TypedDependency;

public class example2 {
	public static void main(String[]args) {
		LexicalizedParser lp = LexicalizedParser.loadModel("edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz","-maxLength", "80","-retainTmpSubcategories");
		TreebankLanguagePack tlp = new PennTreebankLanguagePack();
		tlp.setGenerateOriginalDependencies(true);
		GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();
//		String[]sent= {"this","is","an","easy","sentence","."};
		String sent="This is an easy sentence.";
		Tree parse = lp.parse(sent);
		GrammaticalStructure gs= gsf.newGrammaticalStructure(parse);
		Collection <TypedDependency> tdl= gs.typedDependenciesEnhancedPlusPlus();
		System.out.println(tdl);
}
}