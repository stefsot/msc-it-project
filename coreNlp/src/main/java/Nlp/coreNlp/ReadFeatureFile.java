package Nlp.coreNlp;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReadFeatureFile {
	private int stepCounter;
	private int avgSteps=0;
	private int fileCounter=0;
	private int numberOfSteps=0;
	private static int maxSteps=0;
	private static int minSteps=0;
	private ArrayList <String> gherkinText= new ArrayList();
	private ArrayList<String> daySelection=new ArrayList();
	private static ArrayList<String> biggestFile=new ArrayList();
	private static ArrayList<String> smallestFile=new ArrayList();
	static ArrayList<String[]> scenarios=new ArrayList();
	int i;
	public ReadFeatureFile(List<File> myFile) {
		readFeatureFile(myFile);
		
	}

	public void readFeatureFile(List<File> file) {		
		try {
	    		Random rand = new Random();
	    		int n = rand.nextInt(file.size());
	    		
				fileCounter= file.size();
	    		stepCounter=0;
	    		daySelection.clear();
	                  		for(File myFile:file) {
	                  			//for every file in the list
		    					Path path=myFile.toPath();
		                        List<String> data = Files.readAllLines(path);
		                        String[]arr=new String[data.size()];
		                        //All steps are stored in a String array
		                        i=0;
		                        numberOfSteps=0;
		                        data.forEach(line -> {
		                            Matcher stepMatcher = Pattern.compile("\\b(Given|When|Then|And|But)(.*)").matcher(line);
		                            //Differentiates a step from comments and non step lines
		                            if (stepMatcher.find()) {
		                            	//if the line is a step
		                            	gherkinText.add(line);
		                            	//adds the line to the List
		                            	stepCounter++;
		                            	numberOfSteps++;
		                            	arr[i]=line;
		                            	i++;
//		                                System.out.println(line);
		                            	if(myFile.equals(file.get(n))) {
		                            		daySelection.add(line);
		                            	}
		                            }
		                            
		                        });
		                        boolean bigFile=biggestFile(numberOfSteps);
		                        //Checks if the number of Steps is greater than the number already stored
		                        boolean smallFile=smallestFile(numberOfSteps);
		                      //Checks if the number of Steps is smaller than the number already stored
		                        if(bigFile) {
		                        	setBigg(data);
		                        	//sets new biggest file
		                        }else if(smallFile) {
		                        	setSmall(data);
		                        	//sets new smallest file
		                        }
		                        scenarios.add(arr);
//		                        System.out.println("\n");
	                  		}	
//	                  			System.out.println("Number of steps: "+stepCounter);
	                  			avgSteps=(stepCounter/fileCounter);
	                  		} catch (Exception e) {
	                        
	                        e.printStackTrace();
	                    }
	    
	   
	}

	public ArrayList<String> getGherkinText() {
		return gherkinText;
	}
	
	public int getFileCounter() {
		return fileCounter;
	}
	public int getAvgSteps(){
		return avgSteps;
	}
	public ArrayList<String> getDaySelection(){
		Random rand=new Random();
		int n = rand.nextInt(daySelection.size());
		ArrayList<String> selection = new ArrayList();
//		return daySelection;
		boolean y=true;
		while(y==true) {
			if (n<daySelection.size()){
				if (daySelection.get(n).contains("Given")) {
					selection.add(daySelection.get(n));
					n++;
					boolean x=true;
					while(x==true) {
						if(daySelection.get(n).contains("Then")) {
							selection.add(daySelection.get(n));
							x=false;
						}else {
							selection.add(daySelection.get(n));
							n++;
						}
					}
					y=false;
				}else {
					n++;
				}
			}else {
				n = rand.nextInt(daySelection.size());
			}
		}
		return selection;
	}
	private static boolean biggestFile(int steps) {
		//Checks whether the file has more steps than the one already stored as the biggest
		boolean isBiggest=false;
		if(steps>maxSteps) {
			maxSteps=steps;
			isBiggest=true;
		}
		return isBiggest;
	}
	private static boolean smallestFile(int steps) {
		//Checks whether the file has more steps than the one already stored as the smallest
		boolean isSmallest=false;
		if(steps<minSteps || minSteps==0) {
			minSteps=steps;
			isSmallest=true;
		}
		return isSmallest;
	}
	public static int getMaxSteps() {
		return maxSteps;
	}
	public static int getMinSteps() {
		return minSteps;
	}
	public static ArrayList<String[]> getScenarios(){
		return scenarios;
	}
	public static void setBigg(List<String> myData){
		//Sets new biggest file
		for(String line:myData) {
			biggestFile.add(line);
		}
	}
	public static void setSmall(List<String>dat) {
		for(String line:dat) {
			//sets new smallest file
			smallestFile.add(line);
		}
	}
	public static ArrayList<String> getBiggestFile() {
		return biggestFile;
	}
	public static ArrayList<String> getSmallestFile() {
		return smallestFile;
	}
}
