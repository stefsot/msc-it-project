package Nlp.coreNlp;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.*;
import edu.stanford.nlp.util.CoreMap;

import java.util.*;


//import edu.stanford.nlp.pipeline.CoreDocument;
public class Example {

	public static void main(String[] args) {
		 		
		        // creates a StanfordCoreNLP object, with POS tagging, lemmatization, NER, parsing, and coreference resolution
		        Properties props = new Properties();
		        props.setProperty("annotators", "tokenize, ssplit, pos, lemma, ner, parse, dcoref");
		        StanfordCoreNLP pipeline = new StanfordCoreNLP(props);

		        // read some text in the text variable
		        String text = "What is the weather today?";
		        

		        // create an empty Annotation just with the given text
		        Annotation document = new Annotation(text);

		        // run all Annotators on this text
		        pipeline.annotate(document);
		        CoreMap structure = document.get(CoreAnnotations.CoNLLDepAnnotation.class);
		        System.out.println(structure.get(CoreAnnotations.CoNLLDepTypeAnnotation.class).charAt(0));
		        List<CoreMap> sentences = document.get(CoreAnnotations.SentencesAnnotation.class);
		        for(CoreMap sentence: sentences) {
		          // traversing the words in the current sentence
		          // a CoreLabel is a CoreMap with additional token-specific methods
		          for (CoreLabel token: sentence.get(CoreAnnotations.TokensAnnotation.class)) {
		            // this is the text of the token
		            String word = token.get(CoreAnnotations.TextAnnotation.class);
		            // this is the POS tag of the token
		            String pos = token.get(CoreAnnotations.PartOfSpeechAnnotation.class);
		            // this is the NER label of the token
		            String ne = token.get(CoreAnnotations.NamedEntityTagAnnotation.class);
		            
		            System.out.println(String.format("word: [%s] pos: [%s] ne: [%s]", word, pos, ne));

		          }
		    
		    }

	}
}
