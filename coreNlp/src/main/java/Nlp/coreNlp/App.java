package Nlp.coreNlp;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Properties;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.parser.lexparser.LexicalizedParser;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.trees.GrammaticalStructure;
import edu.stanford.nlp.trees.GrammaticalStructureFactory;
import edu.stanford.nlp.trees.PennTreebankLanguagePack;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreebankLanguagePack;
import edu.stanford.nlp.trees.TypedDependency;
import edu.stanford.nlp.util.CoreMap;

public class App 
{
    public static void main( String[] args ) {
//    	ShellCommand query=new ShellCommand();
	    List<File>myFile= new FileSearch().getResultFile();    
	    ReadFeatureFile myRead= new ReadFeatureFile(myFile);
	    ArrayList<String> bigString=myRead.getBiggestFile();
	    ArrayList<String> smallString=myRead.getSmallestFile();
	    int fileCounter=myRead.getFileCounter();
	    int avgNumberOfSteps=myRead.getAvgSteps();
	    int biggestFile=myRead.getMaxSteps();
	    int smallestFile=myRead.getMinSteps();
	    naturalLanguageProcessing(myRead.getGherkinText(), fileCounter, avgNumberOfSteps,biggestFile,smallestFile);
	    System.out.println("The file with the biggest number of steps has: "+ biggestFile+" steps");
	    System.out.println("The file with the smallest number of steps has: "+ smallestFile+" steps");
	    System.out.println(bigString.get(0));
	    System.out.println(smallString.get(0));
	    System.out.println();
	    try {
	    	writeTxtFile(smallString,"smallFile");
	    	writeTxtFile(bigString,"bigFile");
			writeTxtFile(myRead.getDaySelection(),"scenario");
			writeJsonFile(myRead.getScenarios());
		} catch (IOException e) {
			e.printStackTrace();
		}
	    
	
}

	 public static void naturalLanguageProcessing(ArrayList<String>myTextList, int counter,int average,int myBiggestFile,int smallestFile) {
		// creates a StanfordCoreNLP object, with POS tagging, lemmatization, NER, parsing, and coreference resolution
	        Properties props = new Properties();
	        props.setProperty("annotators", "tokenize, ssplit, pos, lemma, ner, parse, dcoref");
	        StanfordCoreNLP pipeline = new StanfordCoreNLP(props);
	        LexicalizedParser lp = LexicalizedParser.loadModel("edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz","-maxLength", "80","-retainTmpSubcategories");
			TreebankLanguagePack tlp = new PennTreebankLanguagePack();
			tlp.setGenerateOriginalDependencies(true);
			GrammaticalStructureFactory gsf = tlp.grammaticalStructureFactory();
	        String text = "What is the weather?";
	        int passive=0;
	        int active=0;
	        int unknonwn=0;
	        int j=0;
	        for(String line:myTextList) {
	        	//Processes every single line
		        j++;
				Tree parse = lp.parse(line);
				GrammaticalStructure gs= gsf.newGrammaticalStructure(parse);
				Collection <TypedDependency> tdl= gs.typedDependenciesEnhancedPlusPlus();
				if(tdl.toString().contains("nsubjpass")) {
					//Checks the line for active voice
					passive++;
				}else if(tdl.toString().contains("nsubj")) {
					//Checks the line for active voice
					active++;
				}else {
					unknonwn++;
				}
		
		        Annotation document = new Annotation(line);
	
		        // run all Annotators on the lines
		        pipeline.annotate(document);
		        
		        List<CoreMap> sentences = document.get(CoreAnnotations.SentencesAnnotation.class);
	
		        for(CoreMap sentence: sentences) {
		          for (CoreLabel token: sentence.get(CoreAnnotations.TokensAnnotation.class)) {
		            // this is the text of the token  
		            String word = token.get(CoreAnnotations.TextAnnotation.class);
		            // this is the POS tag of the token
		            String pos = token.get(CoreAnnotations.PartOfSpeechAnnotation.class);
		            // this is the NER label of the token
		            String ne = token.get(CoreAnnotations.NamedEntityTagAnnotation.class);
		            //System.out.println(String.format("word: [%s] pos: [%s] ne: [%s]", word, pos, ne));
		          }
	
		    }
	        
	       }
	        System.out.println("The number of files scanned: "+counter);
	        System.out.println("Number of steps per file: "+average);
	        System.out.println("Passive voice was used in: "+passive+" steps out of: "+j+" steps and active voice was used in: "+passive+" steps");
	        System.out.println("There are: "+unknonwn+" unregistered steps");
	        double k= Math.round(((double)(passive)/(double)(j))*100.0);
	        System.out.println("Passive voice is used at: "+(int)k+"% of the steps in the feature files");
	        try {
				writeJsonSimpleDemo("dataGraph",passive,active,unknonwn,counter,average,myBiggestFile,smallestFile);
				//Creates the JSON file push it to gitlab
			} catch (Exception e) {				
				e.printStackTrace();
			}
	 }
	 public static void writeJsonSimpleDemo(String filename,int pass,int act,int unreg, int files,int avg, int big,int small) throws Exception {  
		    //Creates the JSON files
		 	JSONObject sampleObject = new JSONObject();
		    sampleObject.put("passive voice", pass);
		    sampleObject.put("active voice", act);
		    sampleObject.put("unregistered", unreg);
		    sampleObject.put("files", files);
		    sampleObject.put("average number of steps", avg);
		    sampleObject.put("biggest file",big);
		    sampleObject.put("smallest file", small);
		    JSONArray messages = new JSONArray();
		    messages.add(sampleObject);
		    FileWriter file = new FileWriter(filename+".json");
		    System.out.println(messages.toJSONString());
		    file.write(messages.toJSONString());
		    file.flush();
		}
	 public static void writeTxtFile(ArrayList<String> toBeWritten, String name) throws IOException {
		 //Creates the txt file with all the scenarios
		 FileWriter file = new FileWriter(name+".txt");
		 BufferedWriter writer=new BufferedWriter(file);
		 for(String write: toBeWritten) {
//			 file.write(write);
//			 file.flush();
			 writer.write(write);
			 writer.newLine();
			 writer.flush();
		 }
		 writer.close();
	 }
	 public static void writeJsonFile(ArrayList<String[]>myScenarios) throws IOException {
		 JSONArray jsonArray= new JSONArray();

		 for(String[]scenario:myScenarios) {
			 int i=0;
			 JSONObject sampleObject = new JSONObject();
			 for(String myString:scenario) {
				 sampleObject.put(i, myString);
				 i++;
			 }
			 jsonArray.add(sampleObject);
			 
		 }
		 JSONArray messages = new JSONArray();
		 messages.add(jsonArray);
		 FileWriter file = new FileWriter("scenario.json");
		 System.out.println(messages.toJSONString());
		 file.write(messages.toJSONString());
		 file.flush();
	 }
 }
