require 'json'
require 'net/http'
require 'uri'

uri = URI.parse("https://api.github.com/search/code?q=given+created:2019-8-22+in:file+extension:feature+org:github")

request = Net::HTTP::Get.new(uri)
request["Authorisation"] = "token  e98a1936d1001dbf80862c13137a6c3f25d2c65c"

req_options = {
  use_ssl: uri.scheme == "https",
}

response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
  http.request(request)
end
result=JSON.parse(response.body)
puts result["total_count"]
# uri = URI.parse("https://gitlab.com/stefsot/msc-it-project/raw/master/coreNlp/dataGraph.json")
# request = Net::HTTP::Get.new(uri)
# request["Private-Token"] = "dsUx3WCFyyCLk9GwJz8w"
#
# req_options = {
#   use_ssl: uri.scheme == "https",
# }
#
# response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
#   http.request(request)
# end
# x=0
time= Time.now
puts time.day.to_s+"/"+time.month.to_s+"/"+time.year.to_s
# Note: change this to obtain your chart data from some external source
labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July']
data = [
  {
    label: 'First dataset',
    data: Array.new(labels.length) { rand(40..80) },
    backgroundColor: [ 'rgba(255, 99, 132, 0.2)' ] * labels.length,
    borderColor: [ 'rgba(255, 99, 132, 1)' ] * labels.length,
    borderWidth: 1,
  }, {
    label: 'Second dataset',
    data: Array.new(labels.length) { rand(40..80) },
    backgroundColor: [ 'rgba(255, 206, 86, 0.2)' ] * labels.length,
    borderColor: [ 'rgba(255, 206, 86, 1)' ] * labels.length,
    borderWidth: 1,
  }
]
options = { }

send_event('barchart', { labels: labels, datasets: data, options: options })
