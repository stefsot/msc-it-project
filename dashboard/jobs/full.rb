require 'json'
require 'net/http'
require 'uri'

uri = URI.parse("https://api.github.com/search/code?q=given+in:file+extension:feature+org:github")

request = Net::HTTP::Get.new(uri)
request["Authorisation"] = "token  e98a1936d1001dbf80862c13137a6c3f25d2c65c"

req_options = {
  use_ssl: uri.scheme == "https",
}

response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
  http.request(request)
end
time= Time.now
mytime=time.day.to_s+"/"+time.month.to_s+"/"+time.year.to_s
puts mytime
result=JSON.parse(response.body)
puts result["total_count"]
data= "Github has "+result["total_count"].to_s+" feature files"
send_event('full',{title:mytime,text:data})
