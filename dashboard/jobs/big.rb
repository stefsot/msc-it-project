require 'json'
require 'net/http'
require 'uri'
# file = File.read('dataGraph.json')
# data_hash = JSON.parse(file)
# values =data_hash[0].values
# myvalue= values[0]
# puts myvalue
uri = URI.parse("https://gitlab.com/stefsot/msc-it-project/raw/master/coreNlp/dataGraph.json")
request = Net::HTTP::Get.new(uri)
request["Private-Token"] = "dsUx3WCFyyCLk9GwJz8w"

req_options = {
  use_ssl: uri.scheme == "https",
}

response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
  http.request(request)
end
x=0

SCHEDULER.every '10s' do
  result= JSON.parse(response.body)
  values= result[0].values
  myvalue=values[x]
  data= "Has "+myvalue.to_s+" steps!"
  link_to_send=["https://gitlab.com/stefsot/msc-it-project/raw/master/coreNlp/bigFile.txt","https://gitlab.com/stefsot/msc-it-project/raw/master/coreNlp/smallFile.txt"]
  title=["Biggest Feature","Smallest Feature"]
  send_event('big',{title:title[x],text:data,link:link_to_send[x]})
  if x==0
    x=x+1
  else
    x=x-1
  end
end
