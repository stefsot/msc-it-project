require 'json'
require 'net/http'
require 'uri'

uri = URI.parse("https://gitlab.com/stefsot/msc-it-project/raw/master/coreNlp/dataGraph.json")
request = Net::HTTP::Get.new(uri)
request["Private-Token"] = "dsUx3WCFyyCLk9GwJz8w"

req_options = {
  use_ssl: uri.scheme == "https",
}

response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
  http.request(request)
end
result= JSON.parse(response.body)
keyes= result[0].keys
values= result[0].values
# file = File.read('dataGraph.json')
# data_hash = JSON.parse(file)
# keyes= data_hash[0].keys
# values =data_hash[0].values
# puts keyes
# puts values
# labels = [ 'Jan', 'Feb', 'Mar' ]
labels = [keyes[2],keyes[5],keyes[6]]
data = [
  {
    # data: Array.new(5) { rand(30) },
    data: [values[2],values[5],values[6]],
    # data: Array.new(data_hash[0].size){data_hash[0].values},
    backgroundColor: [
      '#F7464A',
      '#46BFBD',
      '#FDB45C',
      '#239B56',
      '#7F8C8D',
    ],
    hoverBackgroundColor: [
      '#FF6384',
      '#36A2EB',
      '#FFCE56',
    ],
  },
]
options = { }

send_event('piechart', { labels: labels, datasets: data, options: options })
